package com.att.display;

import sofia.graphics.TextShape;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.Paint;

// -------------------------------------------------------------------------
/**
 *  Really basic classic that sets the TextShape to fill in the letters. I had
 *  to make this because I could not figure out a way to do it using the
 *  TextShape methods
 *
 *  @author Mark
 *  @version Nov 11, 2014
 */
public class FillableTextShape extends TextShape {

    /**
     * Create a new FillableTextShape object.
     * @param text the text
     * @param x x location
     * @param y y location
     */
    public FillableTextShape(String text, int x, int y) {
        super(text, new PointF(x, y));
    }

    protected Paint getPaint() {
      Paint p = super.getPaint();
      p.setStyle(Style.FILL_AND_STROKE);
      return p;
    }
}