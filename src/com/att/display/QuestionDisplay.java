package com.att.display;

import android.widget.TableRow;
import android.widget.ScrollView;
import android.util.Log;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioGroup;
import android.graphics.Color;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.att.data.Test;
import com.att.data.CircularLinkedList;
import sofia.app.Screen;
import com.att.data.Question;
import android.widget.TextView;
import android.widget.RadioButton;

// -------------------------------------------------------------------------
/**
 *  Displays a question on the screen
 *
 *  @author Mark Wiggans (mmw125)
 *  @version Nov 12, 2014
 */
public class QuestionDisplay extends Screen{

    private Test test;
    private CircularLinkedList<Question> questions;
    private RelativeLayout background;
    private TextView questionText;
    private Button next;
    private Button prev;
    private RadioGroup radioGroup;
    private TestDisplay testDisplay;
    private ScrollView scrollView;
    private TableRow buttonRow;

    /**
     * Place a description of your method here.
     * @param disp where you select a question
     * @param t the test to load
     * @param quest the questions
     */
    public void initialize(TestDisplay disp, Test t, CircularLinkedList<Question> quest){
        this.test = t;
        testDisplay = disp;
        this.questions = quest;
        background.setBackgroundColor(Color.DKGRAY);
        questionText.setBackgroundColor(Color.WHITE);
//        radioGroup.setBackgroundColor(Color.WHITE);
        scrollView.setBackgroundColor(Color.WHITE);
        buttonRow.setBackgroundColor(Color.WHITE);
        loadQuestion(questions.getCurrent());
        if (quest.size() == 1) {
            //Its not necessary to have them there
            next.setVisibility(View.GONE);
            prev.setVisibility(View.GONE);
        }
        if (t.testTaken()) {
            radioGroup.setClickable(false);
        }
    }

    /**
     * Loads the given question into the screen
     * @param question the question to load
     */
    public void loadQuestion(Question question) {
        radioGroup.removeAllViews();
        setTitle(test.getTestName() + " - Question " + (questions.getCurrent().getQuestionNumber() + 1));
        questionText.setText(question.getQuestion());
        for (int i = 0; i < question.getAnswerChoices().size(); i++) {
            RadioButton rad = new RadioButton(this);
            rad.setText(question.getAnswerChoices().get(i));
            final int num = i;
            rad.setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    v.performClick();
                    questions.getCurrent().setSelected(num);
                    testDisplay.update();
                    return true;
                }
            });
            radioGroup.addView(rad);
            if (i == question.getSelectedAnswer()) {
                rad.toggle();
            }

            if (test.testTaken()) {
                if (i == question.getSelectedAnswer() && question.isCorrect()) {
                    rad.setBackgroundColor(Color.GREEN);
                }
                else if(i == question.getSelectedAnswer()) {
                    rad.setBackgroundColor(Color.RED);
                }else if(i == question.getCorrectAnswer()) {
                    rad.setBackgroundColor(Color.GREEN);
                }
            }
        }
        Log.d(LoginWindow.LOG_TAG, "Checking " + question.getSelectedAnswer());
    }

    /**
     * Advances to the next question
     */
    public void nextClicked() {
        questions.next();
        loadQuestion(questions.getCurrent());
    }

    /**
     * Goes to the previous question
     */
    public void prevClicked() {
        questions.previous();
        loadQuestion(questions.getCurrent());
    }
}
