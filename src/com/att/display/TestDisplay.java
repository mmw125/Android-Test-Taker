package com.att.display;

import java.util.Scanner;
import com.att.data.CircularLinkedList;
import android.os.AsyncTask;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import java.util.ArrayList;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.graphics.Color;
import android.widget.ScrollView;
import android.widget.LinearLayout;
import com.att.data.Question;
import android.widget.TextView;
import android.util.Log;
import com.att.data.Test;
import com.att.data.DataHolder.Student;
import com.att.data.FTPConnection;
import sofia.app.Screen;

// -------------------------------------------------------------------------
/**
 *  Displays all of the question in the test the student is taking
 *
 *  @author Mark Wiggans
 *  @version Nov 26, 2014
 */
public class TestDisplay extends Screen {
    private ScrollView background;
    private Test test;
    private FTPConnection ftp;
    private Student stu;
    private LinearLayout linearLayout;
    private ArrayList<Question> questions;
    private Button submit;
    private boolean submitting = false;
    private TextView top;
    private TestDisplay thisDisp;
    private ArrayList<QuestionTextDisplay> textDisplays;

    /**
     * Place a description of your method here.
     * @param student the student who is taking the test
     * @param testIn the test to display
     * @param ftpConnection used to download the test and upload the submission
     */
    public void initialize(Student student, Test testIn, FTPConnection ftpConnection) {
        thisDisp = this;
        test = testIn;
        ftp = ftpConnection;
        stu = student;
        questions = test.getQuestions();
        background.setBackgroundColor(Color.DKGRAY);
        textDisplays = new ArrayList<QuestionTextDisplay>();
        Log.d(LoginWindow.LOG_TAG, "Loading TestDisplay");
        setTitle(test.getTestName());
        addAnswered();
        QuestionTextDisplay text;
        for (int i = 0; i < testIn.getQuestions().size(); i++) {
             text = new QuestionTextDisplay(testIn.getQuestions().get(i), i + 1);
             textDisplays.add(text);
             linearLayout.addView(text);
        }
        if (!test.testTaken()) {
            addSubmit();
        }
        else {
            new ImportSubmission().execute();
        }
    }

    /**
     * Place a description of your method here.
     */
    public void addAnswered() {
        TableRow answeredContainer = new TableRow(this);
        answeredContainer.setBackgroundColor(Color.rgb(169, 224, 169));
        TableLayout.LayoutParams tableRowParams=
            new TableLayout.LayoutParams
            (TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        tableRowParams.setMargins(0, 20, 0, 20);
        answeredContainer.setLayoutParams(tableRowParams);
//        answeredContainer.setBackgroundColor(Color.WHITE);
        linearLayout.addView(answeredContainer);
        top = new TextView(this);
        top.setText("Answered 0/" + questions.size());
        top.setTextSize(35);
        int padding = 20;
        top.setPadding(padding, padding, padding, padding);
        answeredContainer.addView(top);
    }

    /**
     * Place a description of your method here.
     */
    public void addSubmit() {
        TableRow answeredContainer = new TableRow(this);
        TableLayout.LayoutParams tableRowParams=
            new TableLayout.LayoutParams
            (TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        tableRowParams.setMargins(0, 20, 0, 20);
        answeredContainer.setLayoutParams(tableRowParams);
        answeredContainer.setBackgroundColor(Color.WHITE);
        linearLayout.addView(answeredContainer);
        submit = new Button(this);
        submit.setText("Submit test");
        int padding = 20;
        submit.setPadding(padding, padding, padding, padding);
        answeredContainer.addView(submit);
        submit.setOnTouchListener(new ButtonDownListener());
    }

    /**
     * Place a description of your method here.
     */
    public void update() {
        if (test.testTaken()) {
            int correct = 0;
            for (Question question : questions) {
                if (question.isCorrect()) {
                    correct++;
                }
            }
            top.setText("Score " + correct + "/" + questions.size());
            submit.setVisibility(View.GONE);
        }
        else {
            int answered = 0;
            for (Question question : questions) {
                if (question.isAnswered()) {
                    answered++;
                }
            }
            top.setText("Answered " + answered + "/" + questions.size());
        }

        for(QuestionTextDisplay disp : textDisplays) {
            disp.update();
        }
    }

    private class QuestionTextDisplay extends TableRow {
        private Question question;
        private int questionNumber;
        private TextView num;
        public QuestionTextDisplay(Question q, int number) {
            super(thisDisp);
            question = q;
            question.setQuestionNumber(number);
            questionNumber = number;
            this.setBackgroundColor(Color.WHITE);

            num = new TextView(thisDisp);
            this.addView(num);
            num.setText("Question #" + questionNumber);
            num.setTextSize(35);
            int padding = 20;
            num.setPadding(padding, padding, padding, padding);

            TableLayout.LayoutParams tableRowParams=
                new TableLayout.LayoutParams
                (TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
            tableRowParams.setMargins(0, 20, 0, 20);
            this.setLayoutParams(tableRowParams);
            setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    CircularLinkedList<Question> quest = new CircularLinkedList<Question>(questions);
                    quest.advanceTo(question);
                    presentScreen(QuestionDisplay.class, thisDisp, test, quest);
                }
            });
            update();
        }

        public void update() {
            if(test.testTaken()) {
                if (question.isCorrect()) {
                    num.setText(questionNumber + ": Correct");
                    this.setBackgroundColor(Color.rgb(125, 238, 95));
                }
                else {
                    num.setText(questionNumber + ": Not Correct");
                    this.setBackgroundColor(Color.rgb(255, 102, 102));
                }

            }
            else {
                if (question.isAnswered()) {
                    num.setText(questionNumber + ": Answered");
                    this.setBackgroundColor(Color.rgb(169, 224, 169));
                }
                else {
                    num.setText(questionNumber + ": Not Answered");
                    this.setBackgroundColor(Color.rgb(169, 224, 169));
                }
            }
        }
    }

    /**
     *  Write a one-sentence summary of your class here.
     *  Follow it with additional details about its purpose, what abstraction
     *  it represents, and how to use it.
     *
     *  @author Mark Wiggans
     *  @version Nov 28, 2014
     */
    public class ButtonDownListener implements OnTouchListener {

        public boolean onTouch(View v, MotionEvent event){
            v.performClick();
            if (!submitting && event.getAction() == MotionEvent.ACTION_UP){
                submitting = true;
                submit.setEnabled(false);
                new UploadSubmission().execute();
            }
            return true;
        }
    }

    private class ImportSubmission extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Scanner scanner = ftp.getUserSubmission(test, stu);
            while (scanner.hasNext()) {
                String string = scanner.nextLine();
                String[] splitString = string.split(" ");
                try {
                    Log.d(LoginWindow.LOG_TAG, "Checking " + (Integer.parseInt(splitString[0]) - 1));
                    test.getQuestions().get(Integer.parseInt(splitString[0]) - 1).setSelected(Integer.parseInt(splitString[1]));
                }catch(Exception e) {
                    Log.d(LoginWindow.LOG_TAG, "Error parsing submission");
                }

            }
            return null;
        }

        protected void onPostExecute(Void v) {
            new ImportKey().execute();
        }
    }

    private class ImportKey extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Scanner scanner = ftp.getTestKeyScanner(test);
            while (scanner.hasNext()) {
                String string = scanner.nextLine();
                String[] splitString = string.split(" ");
                int questionNumber = Integer.parseInt(splitString[0]);
                test.getQuestions().get(Integer.parseInt(splitString[0])).setCorrect(Integer.parseInt(splitString[1]));
            }
            return null;
        }

        protected void onPostExecute(Void v) {
            test.setTaken(true);
            update();
        }
    }

    private class UploadSubmission extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ftp.uploadTest(test, stu);
            return null;
        }

        protected void onPostExecute(Void v) {
            test.setTaken(true);
            new ImportKey().execute();
        }

    }

}
