package com.att.display;

import android.widget.TableLayout;
import android.graphics.Color;
import android.widget.RelativeLayout;
import com.att.data.Question;
import java.util.Scanner;
import android.os.AsyncTask;
import com.att.data.FTPConnection;
import android.widget.TextView;
import android.widget.Button;
import com.att.data.Test;
import com.att.data.CircularLinkedList;
import sofia.app.Screen;
import java.util.ArrayList;
import com.att.data.DataHolder.Student;
import com.att.data.DataHolder.SchoolClass;

/**
 * A display for selecting which test/assignment you want to take
 *
 *  @author Mark Wiggans (mmw125)
 *  @version Nov 13, 2014
 */
public class TestSelector extends Screen {
    private Student student;
    private CircularLinkedList<SchoolClass> classes;
    private CircularLinkedList<Test> tests;

    private Button classPrev;
    private TextView classDisp;
    private Button classNext;

    private Button testPrev;
    private TextView testDisp;
    private Button testNext;

    private Button ready;

    private FTPConnection ftp;

    private RelativeLayout background;
    private TableLayout contentContainer;

    /**
     * Creates a new TestSelector object with the given parameters
     * @param username the student's username
     * @param schoolClasses the classes the student is in
     * @param ftpConnection what holds all of the data
     */
    public void initialize(String username,
        ArrayList<SchoolClass> schoolClasses, FTPConnection ftpConnection) {
        ftp = ftpConnection;
        this.student = ftp.getDataHolder().getStudentFromUsername(username);
        setTitle(student.getFirstName()+" "+student.getLastName());
        populateClasses(schoolClasses);
        background.setBackgroundColor(Color.DKGRAY);
//        contentContainer.setBackgroundColor(Color.WHITE);
        contentContainer.setBackgroundColor(Color.rgb(212, 223, 212));
    }

    /**
     * Place a description of your method here.
     * @param schoolClasses
     */
    public void populateClasses(ArrayList<SchoolClass> schoolClasses) {
        if (schoolClasses != null && schoolClasses.size() > 0) {
            classes = new CircularLinkedList<SchoolClass>(schoolClasses);
            loadClass();
        }
        else {
            classDisp.setText("You are not enrolled in any classes");
            setClassButtonEnabled(false);
            setTestButtonEnabled(false);
        }
    }

    /**
     * Place a description of your method here.
     */
    public void loadClass() {
        classDisp.setText(classes.getCurrent().getClassName());
        tests = new CircularLinkedList<Test>(classes.getCurrent().getTests());
        loadTest();
    }

    /**
     * Place a description of your method here.
     */
    public void loadTest() {
        if (tests.getCurrent() != null) {
            setTestButtonEnabled(true);
            testDisp.setText(tests.getCurrent().getTestName());
            if (tests.getCurrent().testTaken()) {
                ready.setText("Review Test");
            }else {
                ready.setText("Take Test");
            }
        }
        else {
            setTestButtonEnabled(false);
            testDisp.setText("");
        }
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @param enabled
     */
    public void setClassButtonEnabled(boolean enabled) {
        classPrev.setEnabled(enabled);
        classNext.setEnabled(enabled);
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @param enabled
     */
    public void setTestButtonEnabled(boolean enabled) {
        testPrev.setEnabled(enabled);
        testNext.setEnabled(enabled);
        ready.setEnabled(enabled);
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     */
    public void classNextClicked() {
        classes.next();
        loadClass();
    }

    /**
     * Place a description of your method here.
     */
    public void classPrevClicked() {
        classes.previous();
        loadClass();
    }

    /**
     * Place a description of your method here.
     */
    public void testNextClicked() {
        tests.next();
        loadTest();
    }

    /**
     * Place a description of your method here.
     */
    public void testPrevClicked() {
        tests.previous();
        loadTest();
    }

    /**
     * Place a description of your method here.
     */
    public void readyClicked() {
        setClassButtonEnabled(false);
        setTestButtonEnabled(false);
        ready.setText("Loading. Please wait");
        new LoadTest().execute(tests.getCurrent());
    }

    private class LoadTest extends AsyncTask<Test, Void, Void>{

        @Override
        protected Void doInBackground(Test... vars) {
            if (vars[0] != null) {
                Scanner scanner = ftp.testDirectoryToScanner(vars[0]);
                ArrayList<Question> questions = new ArrayList<Question>();
                while (scanner.hasNext()) {
                    ArrayList<String> questionData = new ArrayList<String>();
                    String string = scanner.nextLine();
                    String[] splitString = string.split(":");
                    if (splitString[0].equals("MUL")) {
                        for (int i = 2; i < splitString.length; i++) {
                            questionData.add(splitString[i]);
                        }
                        questions.add(new Question(splitString[1], questionData));
                    }


                }
                vars[0].setQuestions(questions);
            }
            return null;
        }

        protected void onPostExecute(Void v) {
            presentScreen(TestDisplay.class, student, tests.getCurrent(), ftp);
            setClassButtonEnabled(true);
            setTestButtonEnabled(true);
            ready.setText("Review Test");
        }

    }
}
