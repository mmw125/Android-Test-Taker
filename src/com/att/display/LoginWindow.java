package com.att.display;

import android.graphics.Color;
import sofia.app.Screen;
import android.widget.RelativeLayout;
import android.graphics.*;
import com.att.data.FTPConnection;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.EditText;
import com.att.data.DataHolder.SchoolClass;

/**
 * The window that you use to login to the application
 *
 * @author Mark Wiggans (mmw125)
 * @version Nov 7, 2014
 */
public class LoginWindow extends Screen {
    private EditText      username;
    private EditText      password;
    private FTPConnection ftp;
    private TextView      console;
    private RelativeLayout background;
    private RelativeLayout container;
    /**
     * The tag to put on all of the log outputs
     */
    public static final String LOG_TAG = "ATT";


    public void initialize() {
        ftp = new FTPConnection();

        background.setBackgroundColor(Color.DKGRAY);
//        container.setBackgroundColor(Color.WHITE);
        container.setBackgroundColor(Color.rgb(212, 223, 212));
    }

    /**
     * Logs in using a default username and password
     */
    public void lazyLoginClicked() {
        username.setText("user");
        password.setText("user");
        loginClicked();
    }

    /**
     * Checks the user name and password fields against the database and sees
     * if there is a match
     */
    public void loginClicked()
    {
        if (ftp.getDataHolder() != null
            && ftp.getDataHolder().correctLoginForStudent(
                username.getText().toString(),
                password.getText().toString()))
        {
            console.setText("Logged in");
            new CacheTree().execute(username.getText().toString());
            password.setText("");
            console.setText("Login Successful. Loading...");
        }else {
            console.setText("Not logged in");
        }
    }

    /**
     *  Using a given username, caches all of the classes and assignments in
     *  that class and returns it to the user
     *
     *  @author Mark Wiggans (mmw125)
     *  @version Nov 11, 2014
     */
    private class CacheTree extends AsyncTask<String, Void, ArrayList<SchoolClass>> {
        private String student;

        @Override
        protected ArrayList<SchoolClass> doInBackground(String... students)
        {
            if (students[0] != null) {
                Log.d(LOG_TAG, "Looking for "+students[0]);
                try
                {
                    this.student = students[0];
                    return ftp.getStudentTree(student);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(ArrayList<SchoolClass> classes) {
            presentScreen(TestSelector.class, student, classes, ftp);
        }
    }
}
