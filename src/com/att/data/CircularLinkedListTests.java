package com.att.data;

import java.util.NoSuchElementException;

import student.TestCase;

import java.util.Iterator;

//-------------------------------------------------------------------------
/**
 * Example unit tests for the CircularLinkedList class.
 *
 * @author Mark Wiggans (mmw125)
 * @version 2014.11.03
 */
public class CircularLinkedListTests extends TestCase {
    private CircularLinkedList<String> list;

    /**
     * Creates a brand new, empty CircularLinkedList for each test method.
     */
    public void setUp() {
        list = new CircularLinkedList<String>();
    }

    /**
     * Test if next and previous throw excpetions when they are
     * called on lists without any nodes
     */
    public void testNextAndPrevWithoutElements() {
        Exception e = null;
        try {
            list.next();
            list.previous();
        }
        catch (Exception exception) {
            e = exception;
        }
        assertEquals(null, e);
    }

    /**
     * Tests the next method with zero to two things in it
     */
    public void testAdd() {
        list.add("0");
        list.next();
        assertEquals("0", list.getCurrent());
        assertEquals(1, list.size());
        list.add("1");
        list.previous();
        assertEquals("0", list.getCurrent());
        assertEquals(2, list.size());
    }

    /**
     * Tests the remove current method with no things in the list
     */
    public void testRemoveCurrentZero() {
        Exception exception = null;
        try {
            list.removeCurrent();
        }
        catch (Exception e) {
            exception = e;
        }
        assertNotNull(exception);
        assertTrue(exception instanceof NoSuchElementException);
    }

    /**
     * Tests the remove current method with one thing in the list
     */
    public void testRemoveCurrentOne() {
        list.add("1");
        assertEquals(1, list.size());
        assertEquals("1", list.removeCurrent());
        assertEquals(0, list.size());
    }

    /**
     * Tests the remove current method with one thing in the list
     */
    public void testRemoveCurrentTwo() {
        list.add("2");
        list.add("1");
        assertEquals(2, list.size());
        assertEquals("1", list.removeCurrent());
        assertEquals(1, list.size());
    }

    /**
     * Tests the remove current method with one thing in the list
     */
    public void testToString() {
        list.add("2");
        list.add("1");
        assertEquals("[1, 2]", list.toString());
    }

    /**
     * Tests the remove current method with one thing in the list
     */
    public void testClear() {
        list.add("2");
        list.add("1");
        list.clear();
        assertEquals(0, list.size());
        Exception exception = null;
        try {
            list.getCurrent();
        }
        catch (Exception e) {
            exception = e;
        }
        assertNotNull(exception);
        assertTrue(exception instanceof NoSuchElementException);
    }

    /**
     * Tests the next method inside the iterator
     */
    public void testIteratorNext() {
        list.add("1");
        list.add("2");

        Iterator<String> iterator = list.iterator();
        assertTrue(iterator.hasNext());
        assertEquals("2", iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals("1", iterator.next());
        assertFalse(iterator.hasNext());
        assertEquals(null, iterator.next());
    }

    /**
     * Tests the iterator without any nodes
     */
    public void testIteratorNoNodes() {
        Iterator<String> iterator = list.iterator();
        assertFalse(iterator.hasNext());
        assertEquals(null, iterator.next());
    }

    /**
     * Tests the remove method in the iterator
     */
    public void testIteratorRemove() {
        Exception exception = null;
        try {
            list.iterator().remove();
        }
        catch (Exception e) {
            exception = e;
        }
        assertNotNull(exception);
        assertTrue(exception instanceof UnsupportedOperationException);
    }

}
