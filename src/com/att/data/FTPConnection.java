package com.att.data;

import java.io.PrintWriter;
import com.att.data.DataHolder.Student;
import com.att.data.DataHolder.Teacher;
import java.io.FileNotFoundException;
import java.util.Scanner;
import com.att.data.DataHolder.SchoolClass;
import com.att.display.LoginWindow;
import java.io.FileInputStream;
import org.apache.commons.net.ftp.FTPFile;
import java.util.ArrayList;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import org.apache.commons.net.ftp.FTP;
import android.util.Log;
import android.os.Environment;
import org.apache.commons.net.ftp.FTPClient;
import android.os.AsyncTask;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

/**
 * Write a one-sentence summary of your class here. Follow it with additional
 * details about its purpose, what abstraction it represents, and how to use it.
 *
 * @author Mark
 * @version Nov 8, 2014
 */
public class FTPConnection
{
    private FTPClient  ftpClient;
    private DataHolder holder;


    /**
     * Create a new FTPConnection object.
     */
    public FTPConnection()
    {
        ftpClient = new FTPClient();
        holder = new DataHolder();
        new ConnectToServer().execute();
    }


    /**
     * Gets the data holder if it is done loading
     *
     * @return the data holder if it is done loading
     */
    public DataHolder getDataHolder()
    {
        if (holder.isReady())
        {
            return holder;
        }
        return null;
    }


    /**
     * Place a description of your method here.
     *
     * @param test
     *            the test to upload
     * @param student
     *            the student taking the test
     */
    public void uploadTest(Test test, Student student)
    {
        File download =
            Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(download.getAbsolutePath(), test.getTestName());
        try
        {
            file.createNewFile();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
        PrintWriter pw;
        try
        {
            pw = new PrintWriter(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return;
        }

        for (Question question : test.getQuestions())
        {
            pw.println(question.toString());
        }
        pw.close();
        String serverDirectory =
            test.getTeacher().getUsername() + "/"
                + test.getSchoolClass().getClassname() + "/"
                + test.getTestName() + "/" + student.getUsername() + ".sub";
        try
        {
            ftpClient.storeFile(serverDirectory, new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    // ----------------------------------------------------------
    /**
     * Gets the key file from the given test
     *
     * @param test
     *            the test to get the key file from
     * @return a scanner of the key file
     */
    public Scanner getTestKeyScanner(Test test)
    {
        String path =
            test.getTeacher().getUsername() + "/"
                + test.getSchoolClass().getClassname() + "/"
                + test.getTestName() + "/key.key";
        Log.d(LoginWindow.LOG_TAG, "Downloading: " + path);
        return getScannerForServerFile(path, test.getTestName() + ".key");
    }

    /**
     * Place a description of your method here.
     * @param test the test to the the submission for
     * @param stu the student
     * @return a scanner of the submission file
     */
    public Scanner getUserSubmission(Test test, Student stu)
    {
        String path =
            test.getTeacher().getUsername() + "/"
                + test.getSchoolClass().getClassname() + "/"
                + test.getTestName() + "/" + stu.getUsername() + ".sub";
        Log.d(LoginWindow.LOG_TAG, "Downloading: " + path);
        return getScannerForServerFile(path, test.getTestName() + ".key");
    }


    /**
     * Place a description of your method here.
     *
     * @param serverFileDirectory
     * @param localName
     *            what to name the file
     * @return a scanner scanning the file
     */
    public Scanner getScannerForServerFile(
        String serverFileDirectory,
        String localName)
    {
        File sdCard =
            Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(sdCard.getAbsolutePath(), localName);
        try
        {
            downloadAndSaveFile(serverFileDirectory, file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        try
        {
            return new Scanner(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Given a test, downloads the test from the server and returns a scanner of
     * the test
     *
     * @param test
     *            the test to download
     * @return a scanner of the test file
     */
    public Scanner testDirectoryToScanner(Test test)
    {
        Teacher teacher = test.getTeacher();
        String username = teacher.getUsername();
        String directory =
            username + "/" + test.getSchoolClass().getClassname() + "/"
                + test.getTestName() + "/" + test.getTestName() + ".tst";
        return getScannerForServerFile(directory, test.getTestName() + ".tst");
    }


    private Boolean downloadAndSaveFile(String filename, File localFile)
        throws IOException
    {
        try
        {
            Log.d(LoginWindow.LOG_TAG, "Logged in");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            Log.d(LoginWindow.LOG_TAG, "Downloading");
            Log.d(LoginWindow.LOG_TAG, "Using " + localFile.getAbsolutePath());
            ftpClient.enterLocalPassiveMode();

            OutputStream outputStream = null;
            boolean success = false;
            try
            {
                outputStream =
                    new BufferedOutputStream(new FileOutputStream(localFile));
                success = ftpClient.retrieveFile(filename, outputStream);
            }
            finally
            {
                if (outputStream != null)
                {
                    outputStream.close();
                }
            }

            return success;
        }
        finally
        {
            // does nothing
        }
    }


    /**
     * Uploads the data from the input stream to a file at the given directory
     *
     * @param fileDirectory
     *            Where the data should be stored on the server
     * @param upload
     *            The data to be uploaded
     * @throws IOException
     */
    public void uploadFile(String fileDirectory, FileInputStream upload)
        throws IOException
    {
        System.out.println(fileDirectory);
        ftpClient.storeFile(fileDirectory, upload);
    }


    /**
     * Connects to the server
     *
     * @return if the connection is successful
     */
    public boolean connectToServer()
    {
        Log.d(LoginWindow.LOG_TAG, "Trying to connect to server");
        try
        {
            ftpClient.connect("www.stormsproductions.com");
            Log.d(LoginWindow.LOG_TAG, "Connecting to stormsproductions.com");
        }
        catch (SocketException e)
        {
            Log.d(LoginWindow.LOG_TAG, "Socket exception when connecting");
            e.printStackTrace();
            Log.d(LoginWindow.LOG_TAG, e.getMessage());
        }
        catch (IOException e)
        {
            Log.d(LoginWindow.LOG_TAG, "IOException wneh connecting");
            e.printStackTrace();
            Log.d(LoginWindow.LOG_TAG, e.getMessage());
        }

        try
        {
            Log.d(LoginWindow.LOG_TAG, "Trying to log in");
            return ftpClient.login("projectXman", "projectXman1!");
        }
        catch (IOException e)
        {
            Log.d(LoginWindow.LOG_TAG, "IOException when logging in");
            e.printStackTrace();
            Log.d(LoginWindow.LOG_TAG, e.getMessage());
        }
        return false;
    }


    /**
     * Given information about a class, searches the server for information
     * about assignments for that class and returns a class with the information
     *
     * @param cla
     *            the class to look for
     * @param username
     *            the student's user name
     * @return a class with the given parameters all of the assignments
     */
    public SchoolClass populateClass(SchoolClass cla, String username)
    {
        try
        {
            for (FTPFile file : ftpClient.listFiles(cla.getTeacher()
                .getUsername() + "/" + cla.getClassname()))
            {
                Test test = new Test(cla, file.getName(), false);
                boolean submission = false;
                boolean canTakeTest = false;
                for (FTPFile innerFile : ftpClient.listFiles(cla.getTeacher()
                    .getUsername()
                    + "/"
                    + cla.getClassname()
                    + "/"
                    + file.getName()))
                {
                    if (innerFile.getName().equals("key.key"))
                    {
                        canTakeTest = true;
                        Log.d(LoginWindow.LOG_TAG, "there's a key!");
                    }

                    if (innerFile.getName().equals(username + ".sub"))
                    {
                        submission = true;
                        Log.d(LoginWindow.LOG_TAG, "there's a submission!");
                    }
                }
                if (canTakeTest)
                {
                    cla.add(test);
                }
                test.setTaken(submission);
            }
        }
        catch (Exception e)
        {
            Log.d(LoginWindow.LOG_TAG, "Error looking for test");
        }
        return cla;
    }


    /**
     * Using the student's username,
     *
     * @param username
     *            the student's username
     * @return a list of all of the student classes
     * @throws IOException
     */
    public ArrayList<SchoolClass> getStudentTree(String username)
        throws IOException
    {
// connectToServer();
        ArrayList<SchoolClass> classes = holder.getClassesStudentIsIn(username);
        Log.d(LoginWindow.LOG_TAG, "Creating class with " + classes.size()
            + " classes");
        for (SchoolClass studentClass : classes)
        {
            Log.d(
                LoginWindow.LOG_TAG,
                "Checking " + studentClass.getClassname());
            populateClass(studentClass, username);
        }
        return classes;
    }


    private class ConnectToServer
        extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected Void doInBackground(Void... params)
        {
            connectToServer();
            File sdCard =
                Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = new File(sdCard.getAbsolutePath(), "data.txt");
            try
            {
                downloadAndSaveFile("data.txt", file);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            holder.importDataFile(file);
            return null;
        }
    }




}
