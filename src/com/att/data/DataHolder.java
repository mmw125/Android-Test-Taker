package com.att.data;

import com.att.display.LoginWindow;
import android.util.Log;
import java.util.Vector;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *  Contains all of the data imported from the data.txt held on the server
 *
 *  @author Mark Wiggans
 *  @version Nov 7, 2014
 */
public class DataHolder {
    private Vector<SchoolClass> classes;
    private Vector<Teacher> teachers;
    private Vector<Student> students;
    private boolean ready = false;

    /**
     * Returns a class with the given teacher and className
     * @param teacherUsername teacher's username
     * @param className the name of the class
     * @return the class
     */
    public SchoolClass getClass(String teacherUsername, String className)
    {
        for (SchoolClass currentClass : classes)
        {
            if (currentClass.getTeacher().username.equals(teacherUsername)
                && currentClass.getClassname().equals(className))
            {
                return currentClass;
            }
        }
        return null;
    }

    /**
     * Imports the given data file
     * @param fileIn the file to import
     */
    public void importDataFile(File fileIn) {
        Log.d(LoginWindow.LOG_TAG, "Importing file");
        teachers = new Vector<Teacher>();
        classes = new Vector<SchoolClass>();
        students = new Vector<Student>();
//        file = fileIn;
        Scanner scanner = null;
        try
        {
            scanner = new Scanner(fileIn);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        while (scanner.hasNext())
        {
            String inString = scanner.nextLine();
            if (inString.startsWith("TEACHER")) {
                teachers.add(new Teacher(inString.split("~")));
            }
            if (inString.startsWith("STUDENT")) {
                students.add(new Student(inString.split("~")));
            }
            if (inString.startsWith("CLASS")) {
                classes.add(new SchoolClass(inString.split("~")));
            }
        }
        ready = true;
        scanner.close();
    }

    /**
     * Checks if a data file has been imported
     * @return if the file has been imported
     */
    public boolean isReady() {
        return ready;
    }

    // ----------------------------------------------------------
    /**
     * Checks if the given password is correct for the given username
     * @param username the username to check
     * @param password the password to check
     * @return if it is correct
     */
    public boolean correctLoginForStudent(String username, String password)
    {
        for (Student student : students)
        {
            if (student.getUsername().equals(username))
            {
                try
                {
                    if (PasswordHash.validatePassword(
                        password,
                        student.getPassword()))
                    {
                        return true;
                    }
                }
                catch (NoSuchAlgorithmException e)
                {
                    e.printStackTrace();
                }
                catch (InvalidKeySpecException e)
                {
                    e.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }


    /**
     * Gets the student with the given username
     * @param username the username to search for
     * @return the student
     */
    public Student getStudentFromUsername(String username)
    {
        for (Student student : students)
        {
            if (student.getUsername().equals(username))
            {
                return student;
            }
        }
        return null;
    }

    /**
     * Gets the student with the given username
     * @param username the username to search for
     * @return the student
     */
    public Teacher getTeacherFromUsername(String username)
    {
        for (Teacher teacher : teachers)
        {
            if (teacher.getUsername().equals(username))
            {
                return teacher;
            }
        }
        return null;
    }


    /**
     * Gets the teacher for a given classname that the given student is in
     *
     * @param className
     *            the class
     * @param username
     *            the student
     * @return the teacher name
     */
    public Teacher getTeacherNameFromClassName(String className, String username)
    {
        for (SchoolClass studentClass : getClassesStudentIsIn(username))
        {
            if (studentClass.getClassname().equals(className))
            {
                return studentClass.getTeacher();
            }
        }
        return null;
    }


    /**
     * Gets a list of classes that the given student is in
     *
     * @param username
     *            the student's username
     * @return a list of classes the student is in
     */
    public ArrayList<SchoolClass> getClassesStudentIsIn(String username)
    {
        ArrayList<SchoolClass> classesUserIsIn = new ArrayList<SchoolClass>();
        for (SchoolClass currentClass : classes)
        {
            if (currentClass.isStudentInClass(username))
            {
                classesUserIsIn.add(currentClass);
            }
        }
        return classesUserIsIn;
    }

    // -------------------------------------------------------------------------
    /**
     *  Write a one-sentence summary of your class here.
     *  Follow it with additional details about its purpose, what abstraction
     *  it represents, and how to use it.
     *
     *  @author Mark Wiggans
     *  @version Nov 26, 2014
     */
    public class SchoolClass{

        private ArrayList<Test> tests;
        private String       className;
        private Teacher            teacher;
        private ArrayList<String> studentsInClass;


        // ----------------------------------------------------------
        /**
         * Create a new Class object with the given data
         * @param data the data for the class
         */
        public SchoolClass(String[] data)
        {
            studentsInClass = new ArrayList<String>();
            teacher = getTeacherFromUsername(data[1]);
            className = data[2];
            for (int i = 3; i < data.length; i++)
            {
                studentsInClass.add(data[i]);
            }
            tests = new ArrayList<Test>();
        }

        /**
         * Checks if a student is in the class
         *
         * @param username
         *            the student's username
         * @return if the student is in the class
         */
        public boolean isStudentInClass(String username)
        {
            for (String user : studentsInClass)
            {
                if (user.equals(username))
                {
                    return true;
                }
            }
            return false;
        }


        // ----------------------------------------------------------
        /**
         * Gets the name of a class
         * @return the class name
         */
        public String getClassname()
        {
            return className;
        }


        /**
         * Gets an ArrayList with all of the students in the given class
         *
         * @return an ArrayList with all of the students in the given class
         */
        public ArrayList<Student> getStudentsInClass()
        {
            ArrayList<Student> studentStudents = new ArrayList<Student>();
            for (Student student : students)
            {
                if (this.isStudentInClass(student.getUsername()))
                {
                    studentStudents.add(student);
                }
            }
            return studentStudents;
        }

        /**
         * Gets the tests in the current class
         * @return the tests in the current class
         */
        public ArrayList<Test> getTests() {
            return tests;
        }

        /**
         * Adds a test to the given assignment
         * @param test the test to add
         */
        public void add(Test test)
        {
            tests.add(test);
        }

        public String toString()
        {
            return teacher.getFirstName() + " " + teacher.getLastName() + " "
                + className;
        }

        // ----------------------------------------------------------
        /**
         * Place a description of your method here.
         * @return a the class name
         */
        public String getClassName()
        {
            return className;
        }

        // ----------------------------------------------------------
        /**
         * Place a description of your method here.
         * @return the teacher that teaches the class
         */
        public Teacher getTeacher() {
            return teacher;
        }
    }

    /**
     *  Represents a teacher
     *
     *  @author Mark Wiggans
     *  @version Nov 7, 2014
     */
    public class Teacher
    {
        private String username;
        private String password;
        private String firstName;
        private String lastName;


        /**
         * Create a new Teacher object.
         *
         * @param data all of the teacher's information
         * data[1]: username
         * data[2]: first name
         * data[3]: last name
         * data[4]: password
         */
        Teacher(String[] data) {
            this.username = data[1];
            this.firstName = data[2];
            this.lastName = data[3];
            this.password = data[4];
            Log.d(LoginWindow.LOG_TAG, "Creating a new user");
            Log.d(LoginWindow.LOG_TAG, "Username: " + username);
            Log.d(LoginWindow.LOG_TAG, "Password: " + password);
        }


        // ----------------------------------------------------------
        /**
         * Gets the teacher's username
         *
         * @return the teacher's username
         */
        public String getUsername()
        {
            return username;
        }


        /**
         * Gets the teacher's password
         *
         * @return the teacher's password
         */
        public String getPassword()
        {
            return password;
        }

        /**
         * Gets the teacher's first name
         *
         * @return the teacher's first name
         */
        public String getFirstName()
        {
            return firstName;
        }

        /**
         * Gets the teacher's last name
         *
         * @return the teacher's last name
         */
        public String getLastName()
        {
            return lastName;
        }
    }


    // -------------------------------------------------------------------------
    /**
     *  Represnts a student that goes to the school
     *
     *  @author Mark Wiggans
     *  @version Nov 7, 2014
     */
    public class Student extends Teacher {
        /**
         * Create a new Student object using the given data
         * @param data the data to create the student with
         */
        Student(String[] data) {
            super(data);
        }
    }


    // ----------------------------------------------------------
    /**
     * Gets the name of the student from their username
     * @param username the student's username
     * @return the student's name
     */
    public String[] getName(String username)
    {
        String[] output = new String[2];
        for (Teacher teacher : teachers)
        {
            if (teacher.getUsername().equals(username))
            {
                output[0] = teacher.getFirstName();
                output[1] = teacher.getLastName();
                return output;
            }
        }
        for (Student student : students)
        {
            if (student.getUsername().equals(username))
            {
                output[0] = student.getFirstName();
                output[1] = student.getLastName();
                return output;
            }
        }
        return null;
    }
}
