package com.att.data;

import java.util.ArrayList;
import cs2114.restaurant.CircularList;
import cs2114.restaurant.Node;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A circular, doubly-linked list.
 * @param <E> the type of element stored in the list
 * @author Mark Wiggans (mmw125)
 * @version 2014.11.03
 */
public class CircularLinkedList<E> implements CircularList<E> {

    private Node<E> pointer;
    private int     size;

    /**
     * Creates a new CircularLinkedList object
     */
    public CircularLinkedList() {
        size = 0;
    }

    /**
     * Create a new CircularLinkedList object with the given data
     * @param data the data to put into the list
     */
    public CircularLinkedList(ArrayList<E> data) {
        size = 0;
        if (data != null) {
            for (E element : data) {
                add(element);
                next();
            }
        }
    }

    /**
     * Tries to advance to the given object in the array
     * @param args what to advance to
     * @return if it was successful
     */
    public boolean advanceTo(E args)
    {
        for (int i = 0; i < size; i++) {
            next();
            if (getCurrent().equals(args)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the pointer to the next element
     */
    public void next() {
        if (pointer != null) {
            pointer = pointer.next();
        }
    }

    /**
     * Sets the pointer to the previous element
     */
    public void previous() {
        if (pointer != null) {
            pointer = pointer.previous();
        }
    }

    /**
     * Gets the data from the node that the pointer is pointing to
     *
     * @return the data from that node
     */
    public E getCurrent() {
        if (pointer != null) {
            return pointer.data();
        }
        return null;
//        throw new NoSuchElementException("No elements in the linked list");
    }

    /**
     * Gets how many elements are in the list
     * @return how many elements are in the list
     */
    public int size() {
        return size;
    }

    /**
     * Adds a new item to the list
     * @param item the data for the item
     */
    public void add(E item) {
        Node<E> node = new Node<E>(item);
        if (size == 0) {
            pointer = node;
            pointer.join(pointer);
        }
        else {
            Node<E> previous = pointer.previous();
            previous.split();
            previous.join(node);
            node.join(pointer);
            pointer = node;
        }
        size++;
    }


    /**
     * Removes the selected pobject from the list
     * @return the data from that object
     */
    public E removeCurrent() {
        if (size == 0) {
            throw new NoSuchElementException("No elements to throw");
        }
        else if (size == 1) {
            size = 0;
            Node<E> node = pointer;
            pointer = null;
            return node.data();
        }
        else {
            E returnData = pointer.data();
            size--;
            Node<E> previous = pointer.previous();
            Node<E> next = pointer.next();
            previous.split();
            pointer.split();
            previous.join(next);
            pointer = next;
            return returnData;
        }
    }


    /**
     * Removes all of the objects from the list
     */
    public void clear()
    {
        size = 0;
        pointer = null;
    }

    /**
     * Creates a string representation of the list
     *
     * @return a string representation of the list
     */
    public String toString() {
        Iterator<E> iterator = iterator();
        StringBuilder build = new StringBuilder("[");
        int count = 0;
        while (iterator.hasNext()) {
            if (count != 0) {
                build.append(", ");
            }
            build.append(iterator.next());
            count++;
        }
        build.append("]");
        return build.toString();
    }

    /**
     * Gets the iterator for the list
     * @return the iterator for the list
     */
    public Iterator<E> iterator() {
        return new CircularLinkedListIterator();
    }

    private class CircularLinkedListIterator implements Iterator<E> {
        private Node<E> start;
        private Node<E> current;

        /**
         * Creates a new iterator object
         */
        public CircularLinkedListIterator()
        {
            start = pointer;
            current = null;
        }

        /**
         * Says if the iterator has any more information
         * @return if the iterator has any more info
         */
        public boolean hasNext() {
            if (current == null) {
                return start != null;
            }
            return !current.next().equals(start);
        }

        /**
         * Increments the iterator
         * @return the data from the next node
         */
        public E next() {
            if (hasNext()) {
                if (current == null) {
                    current = start;
                }
                else {
                    current = current.next();
                }
                return current.data();
            }
            return null;
        }

        /**
         * We didn't have to complete this
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}