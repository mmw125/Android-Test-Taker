package com.att.data;

import com.att.data.DataHolder.SchoolClass;
import java.util.ArrayList;
import android.util.Log;
import com.att.display.LoginWindow;
import com.att.data.DataHolder.Teacher;

// -------------------------------------------------------------------------
/**
 *  Write a one-sentence summary of your class here.
 *  Follow it with additional details about its purpose, what abstraction
 *  it represents, and how to use it.
 *
 *  @author Mark
 *  @version Nov 9, 2014
 */
public class Test {
    private Teacher teacher;
    private String testName;
    private boolean testTaken;
    private ArrayList<Question> questions;
    private SchoolClass schClass;

    /**
     * Create a new Test object.
     * @param testName
     * @param testTaken
     * @param schClass the class that this test is in
     */
    public Test(SchoolClass schClass, String testName, boolean testTaken) {
        this.schClass = schClass;
        this.teacher = schClass.getTeacher();
        Log.d(LoginWindow.LOG_TAG, "Created a new test");
        questions = new ArrayList<Question>();
        this.testName = testName;
        this.testTaken = testTaken;
    }

    /**
     * Gets the name of the test
     * @return the name of the test
     */
    public String getTestName() {
        return testName;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return the class
     */
    public SchoolClass getSchoolClass() {
        return schClass;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return the teacher
     */
    public Teacher getTeacher() {
        return teacher;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return the questions in the test
     */
    public ArrayList<Question> getQuestions(){
        return questions;
    }

    // ----------------------------------------------------------
    /**
     * Returns if the test has already been taken by the student
     * @return if the test has already been taken by the student
     */
    public boolean testTaken() {
        return testTaken;
    }

    /**
     * Place a description of your method here.
     * @param taken
     */
    public void setTaken(boolean taken) {
        this.testTaken = taken;
    }

    // ----------------------------------------------------------
    /**
     * Adds a question to the test
     * @param question the question to add
     */
    public void addQuestion(Question question) {
        questions.add(question);
    }

    /**
     * Loads all of the questions for this test
     * @param questions to set
     */
    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return teacher.getFirstName() + " " +
            teacher.getLastName() + " " + testName;
    }
}
