package com.att.data;

import com.att.display.LoginWindow;
import android.util.Log;
import java.util.ArrayList;

// -------------------------------------------------------------------------
/**
 *  Write a one-sentence summary of your class here.
 *  Follow it with additional details about its purpose, what abstraction
 *  it represents, and how to use it.
 *
 *  @author Mark
 *  @version Nov 11, 2014
 */
public class Question {
    private ArrayList<String> answers;
    private String question;
    private int selectedAnswer;
    private int questionNumber;
    private int correctAnswer = -1;

    /**
     * Creates a new Question
     * @param question the question text
     * @param answers the answers
     */
    public Question(String question, ArrayList<String> answers) {
        this(question, answers, -1);
    }

    /**
     * Creates a new Question
     * @param question the question text
     * @param answers the answers
     * @param selected the number of the selected question
     */
    public Question(String question, ArrayList<String> answers, int selected) {
        this.answers = answers;
        this.question = question;
        Log.d(LoginWindow.LOG_TAG, "The question is " + question);
        if (selected > answers.size() - 1 || selected < 0) {
            selectedAnswer = -1;
        }
        else {
            selectedAnswer = selected;
        }
    }

    /**
     * Gets an ArrayList of all of the AnswerChoices
     * @return an ArrayList of the AnswerChoices
     */
    public ArrayList<String> getAnswerChoices(){
        return answers;
    }

    /**
     * Gets the question being asked
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Gets the question being asked
     * @return the question
     */
    public int getSelectedAnswer() {
        return selectedAnswer;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return if the question is answered
     */
    public boolean isAnswered() {
        return selectedAnswer >= 0;
    }

    /**
     * Place a description of your method here.
     * @param questionNumber
     */
    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String toString() {
        return questionNumber + " " + selectedAnswer;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return the question number
     */
    public int getQuestionNumber()
    {
        return questionNumber;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @param i
     */
    public void setSelected(int i)
    {
        Log.d(LoginWindow.LOG_TAG, i + " Selected");
        selectedAnswer = i;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @param correct
     */
    public void setCorrect(int correct) {
        correctAnswer = correct;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return if the question is correct
     */
    public boolean isCorrect() {
        return selectedAnswer != -1 && selectedAnswer == correctAnswer;
    }

    // ----------------------------------------------------------
    /**
     * Place a description of your method here.
     * @return the correct answer
     */
    public int getCorrectAnswer()
    {
        return correctAnswer;
    }
}
